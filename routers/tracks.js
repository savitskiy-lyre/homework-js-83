const express = require('express');
const router = express.Router();
const giveCheckedEssence = require('../utilities/giveCheckedEssence');
const Track = require('../models/Track');
const Album = require('../models/Album');

router.get('/', async (req, res) => {
   if (req.query.album) {
      try {
         const artists = await Track.find({album: req.query.album}).populate('album', 'title');
         res.send(artists);
      } catch (e) {
         res.sendStatus(404)
      }
   } else {
      try {
         const artists = await Track.find().populate('album', 'title');
         res.send(artists);
      } catch (e) {
         res.sendStatus(500)
      }
   }
});

router.get('/:id', async (req, res) => {
   try {
      const albums = await Album.find({author: req.params.id})
      const albumsId = albums.map((album) => {
         return album._id
      })
      const tracks = await Promise.all(albumsId.map((id) => {
         return Track.find({album: id}).populate('album', 'title');
      }))
      const convenientData = [];
      tracks.forEach((arr) => {
         convenientData.push(...arr);
      })
      res.send(convenientData);
   } catch (e) {
      res.sendStatus(404)
   }

});
router.post('/', async (req, res) => {
   const {title, album, duration} = req.body;
   if (!title || !album) return res.sendStatus(400);
   const track = new Track(giveCheckedEssence({title, album, duration}));
   try {
      await track.save();
      res.send(track);
   } catch (e) {
      res.sendStatus(400)
   }
});
module.exports = router;