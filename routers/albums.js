const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const router = express.Router();
const Album = require('../models/Album');

const storage = multer.diskStorage({
   destination: (req, file, cb) => {
      cb(null, config.uploadPath);
   },
   filename: (req, file, cb) => {
      cb(null, nanoid() + path.extname(file.originalname));
   }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
   if (req.query.artist) {
      try {
         const artists = await Album.findOne({author: req.query.artist}).populate('author', 'name');
         res.send(artists);
      } catch (e) {
         res.sendStatus(500)
      }
   } else {
      try {
         const artists = await Album.find().populate('author', 'name');
         res.send(artists);
      } catch (e) {
         res.sendStatus(500)
      }
   }
});
router.get('/:id', async (req, res) => {
   try {
      const artists = await Album.findById(req.params.id).populate('author');
      res.send(artists);
   } catch (e) {
      res.sendStatus(404)
   }

});
router.post('/', upload.single('image'), async (req, res) => {
   const {title, author} = req.body;
   if (!title || !author) return res.sendStatus(400);
   const createdAlbum = {title, author};
   if (req.file) createdAlbum.image = req.file.filename;
   const album = new Album(createdAlbum);
   try {
      await album.save();
      res.send(album);
   } catch (err) {
      res.status(500).send({error: err.message})
   }
});
module.exports = router;