const express = require('express');
const router = express.Router();
const User = require('../models/User');

router.get('/', async (req, res) => {
   try {
      const users = await User.find();
      res.send(users);
   } catch (e) {
      res.sendStatus(500)
   }
});
router.post('/', async (req, res) => {
   const {username, password} = req.body;
   if (!username || !password) return res.sendStatus(400);
   const user = new User({username, password});
   try {
      user.generateToken();
      await user.save();
      res.send(user);
   } catch (err) {
      res.sendStatus(500);
   }
});

router.post('/sessions', async (req, res) => {
   const {username, password} = req.body;
   try {
      const user = await User.findOne({username});
      if (!user) return res.status(401).send({error: 'Username not found'});

      const isMatch = await user.checkPassword(password);
      if (!isMatch) return res.status(401).send({error: 'Password is wrong'});

      user.generateToken();
      await user.save();
      res.send({token: user.token});
   } catch (err) {
      res.sendStatus(500)
   }

});

module.exports = router;