const express = require('express');
const router = express.Router();
const User = require('../models/User');
const TrackHistory = require('../models/TrackHistory');
const Track = require('../models/Track');

router.get('/', async (req, res) => {
   try {
      const tracksHistories = await TrackHistory
        .find()
        .populate('user', 'username')
        .populate('track', 'title');
      res.send(tracksHistories);
   } catch (err) {
      res.sendStatus(500);
   }
})

router.post('/', async (req, res) => {
   const {track_id} = req.body;
   const token = req.get('Authorization');
   if (!track_id) return res.status(400).send({error: "Track cannot be found without ID"});
   if (!token) return res.status(400).send({error: 'Token missing'});
   try {
      const user = await User.findOne({token});
      if (!user) return res.status(401).send({error: 'User not found'});
      const track = await Track.findById(track_id);
      if (!track) return res.status(404).send({error: 'Track not found'});
      const trackHistory = new TrackHistory({user: user._id, track: track._id});
      await trackHistory.save();
      res.send(trackHistory);
   } catch (err) {
      res.sendStatus(500);
   }
});

module.exports = router;