const mongoose = require('mongoose');
const ArtistSchema = new mongoose.Schema({
   name: {
      type: String,
      required: true,
   },
   photo: String,
   description: String,
})

const Artist = mongoose.model('artists', ArtistSchema);

module.exports = Artist;