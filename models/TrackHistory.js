const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const TrackHistorySchema = new mongoose.Schema({
   user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'users',
   },
   track: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'tracks',
   },
   datetime: {
      type: String,
      default: new Date().toISOString(),
      required: true,
   }
})

TrackHistorySchema.plugin(idValidator);
const TrackHistory = mongoose.model("track's_histories", TrackHistorySchema);
module.exports = TrackHistory;