const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const AlbumSchema = new mongoose.Schema({
   title: {
      type: String,
      required: true,
      unique: true,
   },
   author: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'artists',
   },
   date: {
      type: String,
      default: new Date().toISOString(),
   },
   image: String,
})

AlbumSchema.plugin(idValidator);
const Album = mongoose.model('albums', AlbumSchema);
module.exports = Album;